DEV-TRAIN repository

The purpose of this repository is to store the Training Exercises



***** HANGMAN *****

Phases divided by branches

 ENVIRONMENT CONSTRUCTION:

   1 - Basic README.md file into master.

   2 - Basic Ruby tools usage by creating: User interaction, Words Dictionary File.

   3 - Basic rspec addition. It will add TESTS into the environment. Restructured to be testable.

 SOLUTION:

   4 - Start game.
       Random word selection.
       Guess letter sending.
       Input validation.
       Game over.

   5 - Word presentation (already guessed, to be guess).
       Mistakes tracking.
       Lives management (counter, increase/decrease rules, game over by lives, game over word presentation).
