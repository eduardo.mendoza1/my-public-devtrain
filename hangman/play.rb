# frozen_string_literal: true

require './lib/hangman'
require './lib/interplay'

class Play
  def initialize
    @interplay = Interplay.new
  end

  def start
    interplay.greetings
    while interplay.response != '3'
      interplay.menu
      next unless interplay.response == '1'

      play_hangman
    end
    interplay.goodbye
  end

  def play_hangman
    hangman = Hangman.new
    while hangman.letters_left.positive? && hangman.lives_left.positive?
      interplay.your_guess(hangman.letters_left)
      interplay.public_send(hangman.make_guess(interplay.response))
      interplay.wrong_attempts(hangman.wrong_attempts)
      interplay.present_word_lives(hangman.word_discovered, hangman.lives_left)
    end
    hangman.lives_left.positive? ? interplay.congratulations : interplay.commiseration
    interplay.the_word_was(hangman.word)
  end

  private

  attr_reader :interplay
end

Play.new.start
