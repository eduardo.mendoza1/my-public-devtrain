# frozen_string_literal: true

require 'random_word'

describe RandomWord do
  let(:rw) { RandomWord.new }
  
  it "Reads the dictionary file and selects a word from it" do
    expect(rw.word).not_to be_nil 
  end
end
