# frozen_string_literal: true

require 'hangman'

describe Hangman do
  let(:hangman) { Hangman.new }
  
  describe '#' do
    subject { hangman.make_guess(letter) }
    
    context 'When the letter is empty' do
      let(:letter) { '' }
      
      it 'is not valid' do
        expect(subject).to be == :no_valid
      end
    end
    
    context 'When the letter is nil' do
      let(:letter) { nil }
      
      it 'is not valid' do
        expect(subject).to be == :no_valid
      end
    end

    context 'When the letter is a number' do
      ("0".."9").each do |number| 
        let(:letter) { number }  
        it 'is not valid' do
          expect(subject).to be == :no_valid
        end
      end
    end

    context 'When instead of a letter it is a word' do
      (2..1000).each do |mult| 
        let(:letter) { "a" * mult }
        it 'is not valid' do
          expect(subject).to be == :no_valid
        end
      end
    end

    context 'When we provide all lowercase letters' do
      ("a".."z").each do |letter| 
        let(:letter) { letter }
        it 'is must be valid' do
          expect(%i[right_answer no_right]).to include(subject)
        end
      end
    end

    context 'When we provide all uppercase letters' do
      ("A".."Z").each do |letter| 
        let(:letter) { letter } 
        it 'is must be valid' do
          expect(%i[right_answer no_right]).to include(subject)
        end
      end
    end
  end
  
  it "Verify finish after try every letter" do
    [*"a".."z", *"A".."Z"].each { |letter| hangman.make_guess(letter) }
    expect(hangman.letters_left.zero?).to be_truthy
  end   
end
