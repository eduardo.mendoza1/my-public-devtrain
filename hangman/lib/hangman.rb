# frozen_string_literal: true

require 'set'
require './lib/random_word'

class Hangman
  def initialize
    self.word = RandomWord.new.word
    self.used = Set.new
  end

  def make_guess(letter)
    if !valid_letter?(letter)
      :no_valid
    elsif !new_guess?(letter)
      word_has?(letter) ? :already_guessed : :already_tried
    elsif guess_letter(letter)
      :right_answer
    else
      :no_right
    end
  end

  def letters_left
    current_size = word.length
    used.each { |letter| current_size -= word.count(letter) }
    current_size
  end

  def word_discovered
    str = "[^#{used.to_a.join}]"
    word.gsub(Regexp.new(str), '_')
  end

  def wrong_attempts
    used.to_a - word.chars
  end

  def lives_left
    word.length - wrong_attempts.length
  end

  attr_reader :word

  private

  def word_has?(letter)
    word.count(letter).positive?
  end

  def valid_letter?(letter)
    !/^[a-zA-Z]$/.match(letter).nil?
  end

  def new_guess?(letter)
    !used.include?(letter)
  end

  def guess_letter(letter)
    used.add(letter)
    word_has?(letter)
  end

  attr_writer :word
  attr_accessor :used
end
