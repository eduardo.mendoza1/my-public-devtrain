# frozen_string_literal: true

class RandomWord
  def initialize
    self.dictionary = IO.readlines('dictionary.txt').map(&:chomp)
  end

  def word
    dictionary.sample
  end
  
  private

  attr_accessor :dictionary
end
