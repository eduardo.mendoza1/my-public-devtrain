# frozen_string_literal: true

class Interplay
  attr_reader :response

  def greetings
    puts "\n\n*** Lets play Hangman! ***\n\n"
  end

  def menu
    puts "\n\nChoose an option:\n\n"
    puts "\n1 - New game\n"
    puts "\n3 - Exit\n\n"
    @response = gets.chomp
  end

  def your_guess(letters_left)
    puts "There are #{letters_left} letters left\n\nMake your guess: "
    @response = gets.chomp
  end

  def no_valid
    puts 'Not valid input. It must be only one letter'
  end

  def already_guessed
    puts "You already guessed '#{@response}'"
  end

  def already_tried
    puts "It seems you have already try '#{@response}', try a new one"
  end

  def no_right
    puts "No, sorry '#{@response}' is not right, try another letter"
  end

  def right_answer
    puts "Yes, '#{@response}' is right!"
  end

  def wrong_attempts(array)
    puts "\nWrong attempts: #{array}\n" unless array.empty?
  end

  def present_word_lives(word, lives)
    lives_counter = lives.to_s + (lives > 1 ? ' lives' : ' life')
    lives_text = "\nYou have #{lives_counter} left\n"
    puts "\nWord to guess: #{word + lives_text}" if lives.positive?
  end

  def the_word_was(word)
    puts "\n\nThe word was ''#{word}''"
  end

  def congratulations
    puts "\n\nCONGRATULATIONS!!! you have guessed."
  end

  def commiseration
    puts "\n\nSorry, you didn't get it this time."
  end

  def goodbye
    puts "\nGood bye!\n\n"
  end

  private

  attr_writer :response
end
